[Documentação - HOME](../README.md)


#Imagens do Produto (POST /PUT)
| nome | descricao | tipo | obrigatorio |
|------|-----------|------|-------------|
| imagem |URL da imagem | string | sim |
| ordem | ordem crescente de exibição das imagens | int |  |


###Exemplo

```json
    "imagens": [{
    				"imagem": "http://url_da_imagem",
    				"ordem": 1
    			}
   
````


