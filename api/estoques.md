[Documentação - HOME](../README.md)

#estoques - ENDPOINT

###Query parameters

| API |  Descrição |
|------------|---------|
| GET /estoque/?={Offest}&Limit={Limit} |  Retorna o estoque de todos os produtos do fornecedor |
| GET /estoque/?={id} | Retorna o preço do produto |   
| PUT /estoque/{id} |  Atualiza o preços do produto |
 
 

#GET /estoque/?={Offest}&Limit={Limit}
**Retorna o estoque de todos os produtos do fornecedor
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Inf Adicional |
|------------|---------|--------------------------------|
| Offset | Ignora os primeiros { Offset } itens | Integer | |
| Limit | Define a quantidade de registros | Integer | 200 registros |

**Requisição
**Parâmetros da Url
#GET /estoque/{id}
**Retorna o estoque do produto

| Nome | Descrição | Tipo  | Obrigatório|
|------------|---------|--------------------------------|
| id | id do produto no lojista | int | Sim |

###Resposta

| nome | descricao | tipo | 
|------|-----------|------|
| id | id do produto no lojista | int |
| codFabricante | codigo do produto no fornecedor | string |
| estoque | estoque atual no lojista | int |


###Output

```json
{"estoques":[ {
    			"id": 123,
    			"codFabricante":"x01",
    			"estoque ": 47
    		}
    ]
  }
```


#PUT /estoque/{id}
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Obrigatório|
|------------|---------|--------------------------------|
| id | id do produto no lojista | int | Sim |

*Body

| nome | descricao | tipo | 
|------|-----------|------|
| codFabricante | codigo do produto no fornecedor | string |
| estoque | estoque atual no lojista | int |


###Output

```json
      {
        "codFabricante":"x01",
    	"estoque ": 47
      }
   
```
**Resposta
  
 | HTTP Status Code | Descricao | Resposta | json |
 |------|-----------|----------|------------- |
 | 200 | estoque alterado com Sucesso | mensagem | {" preco custo atualizado com sucesso"}|
 | 4xx | Requisição inválida | mensagem | {"categoria nao encontrada"}|
 | 500 | Erro desconhecido.  | mensagem | {"erro deconhecido contate o suporte: suporte@b4commerce.com.br "} |