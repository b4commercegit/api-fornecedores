[Documentação - HOME](../README.md)

#pedidos - ENDPOINT

## GET /
**Listagem de pedidos**

###Query parameters

| Parâmetro  | Default | Descrição                      |
|------------|---------|--------------------------------|
| includes   | null    | Inclusão de tabelas auxiliares |
| page       | 1       | Página da paginação            |
| offset     | 10      | Número de pedidos por página  |


| nome | descricao | tipo | tamanho | mascara | obrigatorio |
|------|-----------|------|---------|---------|-------------|
| id | número do pedido no lojista | int |  11 |
| status | situacao pedido | string |  25 |
| status | desrcição da situação do pedido | string | 80 |
| dataCriacao | Data  e Hora da Criação do pedido | datetime |  
| dataConfirmcao | Data da confirmação do pedido | datetime |  
| subtotal | valor total dos produtos do pedido | float |
| valorDesconto | valor total dos descontos do pedido | float |
| valorTotal | valor total do pedido | float |
| informacoesFiscais | lista de informações fiscais para o produto | lista de * [informacoesFiscais](api/produtos_informacoes_fiscais.md)|

 

###Output

```json
{
	"pedidos": [{
  "id": 0,
  "status": "pagamento-pendente",
  "statusDescricao": "Pagamento Pendente",
  "dataCriacao":"2018-08-08T14:33:06.0000000+00:00",
  "dataConfirmacao":"2018-08-08T14:33:06.0000000+00:00",
  "subtotal":164.00,
  "valorFrete": 15.00,
  "valorDesconto": 0.00,
  "valorTotal": 179.90,
  "cliente": {
    "clienteId": 1,
    "tipoPessoa": "Fisica",
    "tipoSexo": "m",
    "nome": "Ferrmentas Kennedy",
    "cpf_cnpj": "14523644493",
    "rg_ie": "123456",
    "email": "contato@ferramentaskennedy.com.br",
    "telefone": "4133141880",
    "celular": "419999999999",
    "dataNascimento": "1980-01-01",
    "responsavel": null",
    
  },
  "enderecoEntrega":
    {
      "nome": "Trabalho",
      "logradouro": "Av Pres Kennedy",
      "numero": "860",
      "complemento": "null",
      "cep": "80220201",
      "bairro": "reboucas",
      "cidade": "Curitiba",
      "estado": "PR",
      "codigoIbge:"4205407"
    },
   "enderecoCobranca":  
    {
       nome": "Casa",
      "logradouro": "Av Pres Kennedy",
      "numero": "860",
      "complemento": "null",
      "cep": "80220201",
      "bairro": "reboucas",
      "cidade": "Curitiba",
      "estado": "PR",
      "codigoIbge:"4205407"

    },
 	"Entrega": {
      "transportadora": 12,
      "nome": "Correios",
      "prazo": 4,
      "nome": "Correios",
      "dataPrevistaEntrega":"2018-08-08T14:33:06.0000000+00:00",
      "dataMaximaEntrega":"2018-08-10T14:33:06.0000000+00:00",
      "rastreio":"PGR855116666"
      
    
    
  },
  "itens": [
    {
      "id":122,
      "sku": "s01",
      "fornecedor_id":"fs444",
      "nome": "Produto Teste",
      "quantidade": 1,
      "preco": 155.90,
      "brinde":false
     }
   ],
   
   
   
   "documentos": [{
			"tipo": "ordem-compra-lojista-fornecedor",
			"numero": 53664,
			"data": "2018-08-10T14:33:06.0000000+00:00"
		},
		{

			"tipo": "nf-entrada-fornecedor-lojista",
			"numero": 12333,
			"nf_chave": "421004 84684182000157 55 001 000000002 010804210 8 ",
			"xml": "",
			"data": "2018-08-10T14:33:06.0000000+00:00"

		},
		{

			"tipo": "nf-saida-lojista-fornecedor",
			"numero": 12333222,
			"nf_chave": "421004 84684182000157 55 001 000000002 010804210 8 ",
			"xml": "",
			"data": "2018-08-11T14:33:06.0000000+00:00"

		},
		{

			"tipo": "nf-venda-fornecedor-cliente",
			"numero":null,
			"nf_chave":null,
			"xml": null,
			"data": null

		}

	]
  }
}
```
 