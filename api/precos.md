[Documentação - HOME](../README.md)

#precos - ENDPOINT

###Query parameters

| API |  Descrição |
|------------|---------|
| GET /precos/?={Offest}&Limit={Limit} |  Retorna todas o preços  dos produtos  fornecedor |
| GET /precos/?={id} | Retorna o preço do produto |   
| PUT /precos/{id} |  Atualiza o preços do produto |
 
 

#GET /precos/?={Offest}&Limit={Limit}
**Retorna todos os preços dos produtos do fornecedor
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Inf Adicional |
|------------|---------|--------------------------------|
| Offset | Ignora os primeiros { Offset } itens | Integer | |
| Limit | Define a quantidade de registros | Integer | 200 registros |

**Requisição
**Parâmetros da Url
#GET /precos/{id}
**Retorna o preços do produto

| Nome | Descrição | Tipo  | Obrigatório|
|------------|---------|--------------------------------|
| id | id do produto no lojista | int | Sim |

###Resposta

| nome | descricao | tipo | 
|------|-----------|------|
| id | id do produto no lojista | int |
| codFabricante | codigo do produto no fornecedor | string |
| precoCusto | preços de custo informado pelo fornecedor | float |


###Output

```json
"precos":[ {
    			"id": 123,
    			"codFabricante":"x01",
    			"precoCusto ": 30.00
    		}
    ]
```


#PUT /precos/{id}
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Obrigatório|
|------------|---------|--------------------------------|
| id | id do produto no lojista | int | Sim |

*Body

| nome | descricao | tipo | 
|------|-----------|------|
| id | id do produto no lojista | int |
| codFabricante | codigo do produto no fornecedor | string |
| precoCusto | preços de custo informado pelo fornecedor | float |


###Exemplo

```json
    {
    	"codFabricante":"x01",
    	"precoCusto ": 30.00
    }
   
```

**Resposta
  
 | HTTP Status Code | Descricao | Resposta | json |
 |------|-----------|----------|------------- |
 | 200 | preço de custo alterado com Sucesso | mensagem | {" preco custo atualizado com sucesso"}|
 | 4xx | Requisição inválida | mensagem | {"categoria nao encontrada"}|
 | 500 | Erro desconhecido.  | mensagem | {"erro deconhecido contate o suporte: suporte@b4commerce.com.br "} |