[Documentação - HOME](../README.md)


#Dimensoes do Produto com embalagem (POST /PUT)
| nome | descricao | tipo | obrigatorio |
|------|-----------|------|-------------|
| altura | altura em cm | float | sim |
| largura | largura em cm | float | sim |
| comprimento | comprimento em cm | float | sim |
| peso | peso em kg | float | sim |

###Exemplo

```json
    "dimensoes": {
    			"altura": 10.00,
    			"largura": 25.00,
    			"comprimento": 30.00,
    			"peso": 1.54
    		}
   
````


