[Documentação - HOME](../README.md)

#categorias - ENDPOINT
*Após a inclusão/alteração das categorias será necessário realizar o "de-para" das categorias do fornecedor com as dos lojista.

###Query parameters

| API |  Descrição |
|------------|---------|
| GET /categorias/?={Offest}&Limit={Limit} |  Retorna todas as categorias do fornecedor |
| GET /categorias/?={codigo} | Retorna informações da categoria |   
| POST /categorias | Cadastra um nova categoria |
| PUT /categorias/{codigo} |  Atualiza as informações de uma categoria |
| DELETE/categorias/{codigo} | remove a categoria  |
 
 

#GET /categorias/?={Offest}&Limit={Limit}
**Retorna todss as categorias do fornecedor
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Inf Adicional |
|------------|---------|--------------------------------|
| Offset | Ignora os primeiros { Offset } itens | Integer | |
| Limit | Define a quantidade de registros | Integer | 200 registros |

**Requisição
**Parâmetros da Url
#GET /categorias/{codigo}
**Retorna informações sobre um produto cadastrado

| Nome | Descrição | Tipo  | Obrigatório|
|------------|---------|--------------------------------|
| codigo | código da categoria do fornecedor | string | Sim |

###Resposta

| nome | descricao | tipo | 
|------|-----------|------|
| codigo | codigo da categoria | string |
| nome | nome da categoria |  string |
| categoriaPai | código  da categoria  pai | string |

###Output

```json
{
	"categorias": [{
		"codigo":"sub01",
		"nome":"Catgeoria Teste",
		"categoriaPai": "01"
		
	}]
}
```

#POST /categorias
**Cadastra uma nova categoria
**Requisição
**Parâmetros da Url
Nenhum
*Body
| nome | descricao | tipo | obrigatorio |
|------|-----------|------|-------------|
| codigo | codigo da categoria | string | sim |
| nome | nome da categoria |  string | sim |
| categoriaPai | código  da categoria  pai | string | |

###Exemplo

```json
    {
		"codigo":"sub01",
        "nome":"Catgeoria Teste",
        "categoriaPai": "01"
	}
````


**Resposta

 | HTTP Status Code | Descricao | Resposta | json |
 |------|-----------|----------|------------- |
 | 201| categoria cadastrada com sucesso | mensagem | {"categoria cadastrada com sucesso"}|
 | 4xx | Requisição inválida | mensagem | {"a categoria já está cadastrada"} |
 | 500 | Erro desconhecido.  | mensagem | {"erro deconhecido contate o suporte: suporte@b4commerce.com.br"} |
 	
#PUT /categorias/{codigo}
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Obrigatório|
|------------|---------|--------------------------------|
| codigo | código da categoria do fornecedor | string | Sim |

 
*Body


| nome | descricao | tipo | 
|------|-----------|------|
| nome | nome da categoria |  string |
| categoriaPai | código  da categoria  pai | string |

###Exemplo

```json
	
        {
    	   "nome":"Catgeoria Teste",
           "categoriaPai": "01"
    	}
````
**Resposta
  
 | HTTP Status Code | Descricao | Resposta | json |
 |------|-----------|----------|------------- |
 | 200 | categoria alterada com Sucesso | mensagem | {"categoria atualizada com sucesso"}|
 | 4xx | Requisição inválida | mensagem | {"categoria nao encontrada"}|
 | 500 | Erro desconhecido.  | mensagem | {"erro deconhecido contate o suporte: suporte@b4commerce.com.br "} |

 #DELETE/categorias/{codigo}
 **Requisição
 **Parâmetros da Url
 
 | Nome | Descrição | Tipo  | Obrigatório|
 |------------|---------|--------------------------------|
 | codigo | código da categoria do fornecedor | string | Sim |
 
  
 *Body
  N/A
 
 ###Exemplo

 **Resposta
   
  | HTTP Status Code | Descricao | Resposta | json |
  |------|-----------|----------|------------- |
  | 200 | categoria removida com Sucesso | mensagem | {"categoria atualizada com sucesso"}|
  | 4xx | Requisição inválida | mensagem | {"categoria nao encontrada"}|
  | 500 | Erro desconhecido.  | mensagem | {"erro deconhecido contate o suporte: suporte@b4commerce.com.br "} |
 
  

