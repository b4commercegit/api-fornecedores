[Documentação - HOME](../README.md)

#produtos - ENDPOINT


###Query parameters

| API |  Descrição |
|------------|---------|
| GET /produtos/?={Offest}&Limit={Limit} |  Retorna todos os produtos do fornecedor |
| GET /produtos/?={id} | Retorna informações do produto  informado |   
| POST /produtos | Cadastra um novo produto |
| PUT /produtos/{codigo} |  Atualiza as informações do produto |
| DELETE/produtos/{codigo} | Atualiza o status do produto para inativo |
 
 

#GET /produtos/?={Offest}&Limit={Limit}
**Retorna todos os produtos
**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Inf Adicional |
|------------|---------|--------------------------------|
| Offset | Ignora os primeiros { Offset } itens | Integer | |
| Limit | Define a quantidade de registros | Integer | 200 registros |

**Requisição
**Parâmetros da Url
#GET /produtos/{codigo}
**Retorna informações sobre um produto cadastrado

| Nome | Descrição | Tipo  | Obrigatório|
|------------|---------|--------------------------------|
| id | id do produto no lojista | int | Sim |

###Resposta

| nome | descricao | tipo | 
|------|-----------|------|
| id | id do produto no lojista | string |
| situacao | Produto ativo ou Inativo |  boolean |
| sku | código sku do produto no lojista | string |
| codFabricante | código do produto no fabricante | string |
| nome | nome do produto | string |
| tituloSeo| titulo do produto | string | 
| subtitulo | subtitulo do produto | string | 
| descricao | descrição do produto | string |
| metaDescricao | meta description do produto | string |
| codigoGrupo | id do grupo | string |
| codigoCategoria | codigo da categoria | string |
| codigoSubcategoria | codigo da subcategoria | string |
| codigoMarca | codigo da marca o  | string |
| palavrasChave | palavras chave para o produto | string |
| ean | código de barras do produto | string |
| modelo | modelo do produto | string |
| referencia | referencia do produto no fabricante | string |
| estoque | estoque total do produto(caso não deseje informar o estoque real informar estoque= 1 disponível e estoque=0 para indisponível )  | int |   
| dimensoes | lista o peso as dimensões do produto com a embalagem |  lista de * [dimensoes](api/produtos_dimensoes.md) | 
| imagens | lista de imagens do produto| lista de * [imagens](api/produtos_imagens.md)|
| atributos | lista de atributos do produto| lista de * [atributos](api/produtos_atributos.md)|
| unidadeVenda | unidade de venda para o produto | string |
| precoCusto | preço de custo do produto para o cliente | float  |
| garantia | identificador do tipo de garantia do produto | int |
| crossDocking | informa se o produto está disponípivel para crossdocking | string |
| informacoesFiscais | lista de informações fiscais para o produto | lista de * [informacoesFiscais](api/produtos_informacoes_fiscais.md)|

###Output

```json
{
	"produtos": [{
		"id": 1236554,
		"situacao": 1,
		"codFabricante": "x01",
		"sku": "43893",
		"nome": " Furadeira Gsb550-Re 550W",
		"tituloSeo": "Furadeira de Impacto Reversível Variável Gsb550-Re 550W",
		"subtitulo": "Potencia e precis\u00E3o",
		"descricao": "Conforto e Produtividade: Formato compacto e\r\n\t\t\tergon\u00F4nico facilita o trabalho e evita o cansa\u00E7o mesmo durante o uso\r\n\t\t\tprolongado -\r\n\t\t\tRobusta: Resistente mandril de 13 mm feito em metal de alta qualidade -\r\n\t\t\t\tControle ideal: Interruptor eletr\u00F4nico com velocidade vari\u00E1vel. -\r\n\t\t\t\tBot\u00E3o - Trava: Excelentes para trabalhos cont\u00EDnuos.",
		"metaDescricao": "Furadeira Bosch em Oferta. Confira!",
		"codigoGrupo": "1",
		"codigoCategoria": "106",
		"codigoSubcategoria": "1006",
		"codigoMarca": "1",
		"palavrasChave": "FURADEIRA,FURADEIRA ELETRICA",
		"modelo": "Gsb550-Re",
		"referencia": "S0123",
		"ean": "7891009828657",
		"estoque": 1,
		"dimensoes": {
			"altura": 10,
			"largura": 25,
			"comprimento": 30,
			"peso": 1000
		},
		"atributos": [
		     {
        		"nome": "Tensão",
        		"valor": "127 volts"
        	},  
          {
        		"nome": "Golpes por minuto",
        		"valor": "0-49.300 IPM (BPM)"
        	}		
		],
		"imagens": [{
				"imagem": "http://url_da_imagem",
				"ordem": 1
			},
			{
				"imagem": "http://url_da_imagem",
				"ordem": 2
			}
		],
		"unidade_de_venda": "UN",
		"precoCusto": 104.96,
		"garantia": 12,
		"cross_docking": false,
		"informacoesFiscais": {
			"ipi": 8.5,
			"cfop": "6108",
			"ncm": "84671110",
			"cest": "806",
			"cst": "5",
			"icms": 12.00,
			"reducao_icms": 0,
			"icms_st": 15.00,
			"mva": 2.00,
			"origem": 1
		}
	}]
}
```

#POST /produtos
**Cadastra informações de um novo produto
**Requisição
**Parâmetros da Url
Nenhum
*Body

| nome | descricao | tipo | obrigatorio |
|------|-----------|------|
| codFabricante | código do produto no fabricante | string | sim |
| situacao | Produto ativo ou Inativo |  boolean | sim |
| nome | nome do produto | string | sim |
| tituloSeo| titulo do produto | string | 
| subtitulo | subtitulo do produto | string | 
| descricao | descrição do produto | string |
| metaDescricao | meta description do produto | string |
| codigoGrupo | id do grupo | string |
| codigoCategoria | codigo da categoria | string |
| codigoSubcategoria | codigo da subcategoria | string |
| codigoMarca | codigo da marca o  | string |
| palavrasChave | palavras chave para o produto | string |
| ean | código de barras do produto | string |
| modelo | modelo do produto | string |
| referencia | referencia do produto no fabricante | string |
| estoque | estoque total do produto(caso não deseje informar o estoque real informar estoque= 1 disponível e estoque=0 para indisponível )  | int |   
| dimensoes | lista peso as dimensões do produto para o envio |  lista de * [dimensoes](api/produtos_dimensoes.md) | 
| imagens | lista de imagens do produto| lista de * [imagens](api/produtos_imagens.md)|
| atributos | lista de atributos do produto| lista de * [atributos](api/produtos_atributos.md)|
| unidadeVenda | unidade de venda para o produto | string |
| precoCusto | preço de custo do produto para o cliente | float  |
| garantia | identificador do tipo de garantia do produto | int |
| crossDocking | informa se o produto está disponípivel para crossdocking | string |
| informacoesFiscais | lista de informações fiscais para o produto | lista de * [informacoesFiscais](api/produtos_informacoes_fiscais.md)|


###Exemplo

```json
{
	"situacao": 1,
	"codFabricante": "x01",
	"nome": " Furadeira Gsb550-Re 550W",
	"tituloSeo": "Furadeira de Impacto Reversível Variável Gsb550-Re 550W",
	"subtitulo": "Potencia e precis\u00E3o",
	"descricao": "Conforto e Produtividade: Formato compacto e\r\n\t\t\tergon\u00F4nico facilita o trabalho e evita o cansa\u00E7o mesmo durante o uso\r\n\t\t\tprolongado -\r\n\t\t\tRobusta: Resistente mandril de 13 mm feito em metal de alta qualidade -\r\n\t\t\t\tControle ideal: Interruptor eletr\u00F4nico com velocidade vari\u00E1vel. -\r\n\t\t\t\tBot\u00E3o - Trava: Excelentes para trabalhos cont\u00EDnuos.",
	"metaDescricao": "Furadeira Bosch em Oferta. Confira!",
	"codigoGrupo": "1",
	"codigoCategoria": "106",
	"codigoSubcategoria": "1006",
	"codigoMarca": "1",
	"palavrasChave": "FURADEIRA,FURADEIRA ELETRICA",
	"modelo": "Gsb550-Re",
	"referencia": "S0123",
	"ean": "7891009828657",
	"estoque": 1,
	"dimensoes": {
		"altura": 10,
		"largura": 25,
		"comprimento": 30,
		"peso": 1000
	},
	"atributos": [{
			"nome": "Tensão",
			"valor": "127 volts"
		},
		{
			"nome": "Altura",
			"valor": "200mm"
		},
		{
			"nome": "Comprimento",
			"valor": "400mm"
		},
		{
			"nome": "Largura",
			"valor": "150mm"
		},
		{
			"nome": "Peso",
			"valor": "1,2kg"
		}
	],
	"imagens": [{
			"imagem": "http://url_da_imagem",
			"ordem": 1
		},
		{
			"imagem": "http://url_da_imagem",
			"ordem": 2
		}
	],
	"unidade_de_venda": "UN",
	"precoCusto": 104.96,
	"garantia": 12,
	"cross_docking": false,
	"informacoesFiscais": {
		"ipi": 8.5,
		"cfop": "6108",
		"ncm": "84671110",
		"cest": "806",
		"cst": "5",
		"icms": 12.00,
		"reducao_icms": 0,
		"icms_st": 15.00,
		"mva": 2.00,
		"origem": 1
	}
}
```
**Resposta

| HTTP Status Code | Descricao | Resposta | json |
 |------|-----------|----------|------------- |
 | 201 | Produto cadastrado com Sucesso | id do produto no lojista | {1254|
 | 4xx | Requisição inválida | mensagem | {"os campo IPI e obrigatorio"} |
 | 500 | Erro desconhecido.  | mensagem | {"erro deconhecido contate o suporte: suporte@b4commerce.com.br "} |
 
 
 	
#PUT /produtos/{id}
 *Caso sejam enviados no request imagens e/ou atributos, aos valores atuais serão substituidos pelos fornecidos. 

**Requisição
**Parâmetros da Url

| Nome | Descrição | Tipo  | Obrigatório|
|----- |---------|------|--------------|
| id | id do produto no lojista  | int | Sim |

 
*Body


| nome | descricao | tipo | obrigatorio |
|------|-----------|------|
| codFabricante | código do produto no fabricante | string | sim |
| situacao | Produto ativo ou Inativo |  boolean | sim |
| nome | nome do produto | string | sim |
| tituloSeo| titulo do produto | string | 
| subtitulo | subtitulo do produto | string | 
| descricao | descrição do produto | string |
| metaDescricao | meta description do produto | string |
| codigoGrupo | id do grupo | string |
| codigoCategoria | codigo da categoria | string |
| codigoSubcategoria | codigo da subcategoria | string |
| codigoMarca | codigo da marca o  | string |
| palavrasChave | palavras chave para o produto | string |
| ean | código de barras do produto | string |
| modelo | modelo do produto | string |
| referencia | referencia do produto no fabricante | string |
| estoque | estoque total do produto(caso não deseje informar o estoque real informar estoque= 1 disponível e estoque=0 para indisponível )  | int |   
| dimensoes | lista peso as dimensões do produto para o envio |  lista de * [dimensoes](api/produtos_dimensoes.md) | 
| imagens | lista de imagens do produto| lista de * [imagens](api/produtos_imagens.md)|
| atributos | lista de atributos do produto| lista de * [atributos](api/produtos_atributos.md)|
| unidadeVenda | unidade de venda para o produto | string |
| precoCusto | preço de custo do produto para o cliente | float  |
| garantia | identificador do tipo de garantia do produto | int |
| crossDocking | informa se o produto está disponípivel para crossdocking | string |
| informacoesFiscais | lista de informações fiscais para o produto | lista de * [informacoesFiscais](api/produtos_informacoes_fiscais.md)|


###Exemplo

````
json
{
	"situacao": 1,
	"codFabricante": "x01",
	"sku": "43893",
	"nome": " Furadeira Gsb550-Re 550W",
	"tituloSeo": "Furadeira de Impacto Reversível Variável Gsb550-Re 550W",
	"subtitulo": "Potencia e precis\u00E3o",
	"descricao": "Conforto e Produtividade: Formato compacto e\r\n\t\t\tergon\u00F4nico facilita o trabalho e evita o cansa\u00E7o mesmo durante o uso\r\n\t\t\tprolongado -\r\n\t\t\tRobusta: Resistente mandril de 13 mm feito em metal de alta qualidade -\r\n\t\t\t\tControle ideal: Interruptor eletr\u00F4nico com velocidade vari\u00E1vel. -\r\n\t\t\t\tBot\u00E3o - Trava: Excelentes para trabalhos cont\u00EDnuos.",
	"metaDescricao": "Furadeira Bosch em Oferta. Confira!",
	"codigoGrupo": "1",
	"codigoCategoria": "106",
	"codigoSubcategoria": "1006",
	"codigoMarca": "1",
	"palavrasChave": "FURADEIRA,FURADEIRA ELETRICA",
	"modelo": "Gsb550-Re",
	"referencia": "S0123",
	"ean": "7891009828657",
	"estoque": 1,
	"dimensoes": {
		"altura": 10.00,
		"largura": 25.00,
		"comprimento": 30.00,
		"peso": 1.52
	},
	"atributos": [{
			"nome": "Tensão",
			"valor": "127 volts"
		},
		{
			"nome": "Altura",
			"valor": "200mm"
		},
		{
			"nome": "Comprimento",
			"valor": "400mm"
		},
		{
			"nome": "Largura",
			"valor": "150mm"
		},
		{
			"nome": "Peso",
			"valor": "1,2kg"
		}
	],
	"imagens": [{
			"imagem": "http://url_da_imagem",
			"ordem": 1
		},
		{
			"imagem": "http://url_da_imagem",
			"ordem": 2
		}
	],
	"unidade_de_venda": "UN",
	"precoCusto": 104.96,
	"garantia": 12,
	"cross_docking": false,
	"informacoesFiscais": {
		"ipi": 8.5,
		"cfop": "6108",
		"ncm": "84671110",
		"cest": "806",
		"cst": "5",
		"icms": 12.00,
		"reducao_icms": 0,
		"icms_st": 15.00,
		"mva": 2.00,
		"origem": 1
	}
}
````
**Resposta
 
 | HTTP Status Code | Descricao | Resposta | json |
 |------|-----------|----------|------------- |
 | 200 | Produto atualizado com Sucesso | mensagem | {"produto atualizado com sucesso"}|
 | 4xx | Requisição inválida | mensagem | {"os campo IPI e obrigatorio"} |
 | 500 | Erro desconhecido.  | mensagem | {"erro deconhecido contate o suporte: suporte@b4commerce.com.br "} |



