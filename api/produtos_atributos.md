[Documentação - HOME](../README.md)


#Informações Fiscais do Produto  (POST /PUT)
*Os dados devem contemplar as exigências fiscais do UF do lojista
| nome | descricao | tipo | obrigatorio |
|------|-----------|------|-------------|
| ipi | IPI do produto | float | nao |
| cfop | cfop do produto | string | sim |
| ncm | ncm do produto | string | sim |
| cest | cest do produto | string | nao |
| cst | cst do produto | string | nao |
| icms | % de icms do produto | float | sim |
| reducao_icms | % de redução de icms do produto | float | nao |
| icms_st | % de icms de subs tributária do produto | float | nao |
| mva | % de mva do produto | float | nao |
| origem | código da origem produto | int | sim |



###Exemplo

```json
    "informacoesFiscais": {
    		"ipi": 8.5,
    		"cfop": "6108",
    		"ncm": "84671110",
    		"cest": "806",
    		"cst": "5",
    		"icms": 12.00,
    		"reducao_icms": 0,
    		"icms_st": 15.00,
    		"mva": 2.00,
    		"origem": 1
   	}
   
```


